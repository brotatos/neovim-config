return {
    'nvim-tree/nvim-tree.lua',
    dependencies = 'nvim-tree/nvim-web-devicons',
    cmd = "NvimTreeToggle",
    keys = {
        { "<leader>t", "<cmd>NvimTreeToggle<cr>", desc="NvimTreeToggle"}
    },

    opts = {
        sort_by = "case_sensitive",
        -- sync_root_with_cwd = true,
        -- reload_on_bufenter = true,
        -- respect_buf_cwd = true,
        update_focused_file = {
            enable = true,
            update_root = true
        }
    }
}
