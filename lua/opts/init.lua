local opt = vim.opt

opt.hidden         = true
opt.splitbelow     = true
opt.splitright     = true
opt.smarttab       = true
opt.expandtab      = true
opt.smartindent    = true
opt.autoindent     = true
opt.backup         = false
opt.writebackup    = false
opt.wrap           = false

opt.pumheight      = 10
opt.updatetime     = 300
opt.timeoutlen     = 500
opt.conceallevel   = 0
opt.tabstop        = 4
opt.shiftwidth     = 4
opt.cmdheight      = 1
opt.number         = true
opt.ignorecase     = true
opt.smartcase      = true
opt.cursorline     = true
opt.title          = true
opt.hlsearch       = true
opt.showmode       = true
opt.ruler          = true
opt.showcmd        = true
opt.mouse          = 'a'
vim.o.clipboard    = "unnamedplus"

vim.g.mapleader = " "
vim.opt.termguicolors = true
opt.guifont = { "JetBrains Mono NL Nerd Font", ":h12" }
--opt.guifont = { "Source Code Pro", ":h12" }
